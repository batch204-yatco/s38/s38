const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// JSON Web Tokens

// Token Creation 
module.exports.createAccessToken = (user) => {

	console.log(user)
	/*
		{
		  _id: new ObjectId("634015b3b80ce95b6dc704a5"),
		  firstName: 'Jane',
		  lastName: 'Hufano',
		  email: 'janehufano@gmail.com',
		  password: '$2b$10$mR3XUn64sPTWaodrYr7T2.rSXHJPOKQIbbFMVr9ZeUsleVLpG/UOO',
		  isAdmin: true,
		  mobileNo: '09123456789',
		  enrollments: [],
		  __v: 0
		}
	*/

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// Generate a token
		// jwt.sign(<payload>)
	return jwt.sign(data, secret, {expiresIn: "1m"})
}