const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")

// Route for checking if user's email already exists in database

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Retrieve User Details
router.post("/details", (req, res) => {
	console.log(req.body)
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))
})

module.exports = router;