
/*npm init -y
install express mongoose cors
npm start (if nodemon index.js is placed in package.json)*/


const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors")

// allow us to access routes
const userRoutes = require("./routes/userRoutes")

const port = 4000;
const app = express();

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin123@batch204-yatcodaphne.rlp0fjo.mongodb.net/s37-s41?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

// to allow resources to access backend app
app.use(cors());
app.use(express.json());

/*localhost:4000/users/checkEmail*/
app.use("/users", userRoutes)

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
})

// Json Web Token is generated when we log in and is used as authorization if we are allowed to send a request 
// Info is verified through secret

/*
JWT Anatomy
	- Header 
	- Payload - contains claims or data about the entity
	- Signature - proof token has not been tampered
*/

// npm install bcrypt
// npm install jsonwebtoken
